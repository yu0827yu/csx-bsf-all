package com.yh.csx.bsf.demo.bigdata;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.yh.csx.bsf.bigdata.BigDataSource;
import com.yh.csx.bsf.core.db.DbHelper;
import com.yh.csx.bsf.core.util.ContextUtils;
import lombok.var;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;


@SpringBootApplication(exclude = { DruidDataSourceAutoConfigure.class,DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class })
public class ClickHouseApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClickHouseApplication.class, args);
        var r = DbHelper.get(BigDataSource.getDefaultClickHouseDataSource(),(c)->{
            return c.executeList("select * from apply_order limit 0,10",null);
        });
        var a=1;
    }
}
