package com.yh.csx.bsf.bigdata;

import com.yh.csx.bsf.core.util.ContextUtils;

import javax.sql.DataSource;

public class BigDataSource {

    public static DataSource getDefaultClickHouseDataSource(){
        return ContextUtils.getBean(DataSource.class,"clickHouseDataSource",true);
    }
}
