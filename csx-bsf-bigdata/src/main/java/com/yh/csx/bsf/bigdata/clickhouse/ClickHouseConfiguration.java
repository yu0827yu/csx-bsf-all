package com.yh.csx.bsf.bigdata.clickhouse;

import com.yh.csx.bsf.core.util.PropertyUtils;
import com.yh.csx.bsf.core.util.ReflectionUtils;
import lombok.var;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ConditionalOnProperty(name = "bsf.clickhouse.enabled", havingValue = "true")
public class ClickHouseConfiguration {
    @Bean("clickHouseDataSource")
    @ConfigurationProperties("spring.datasource.clickhouse")
    public DataSource clickHouseDataSource(){
        var type = ReflectionUtils.classForName(PropertyUtils.getPropertyCache("spring.datasource.clickhouse.type",""));
        return DataSourceBuilder.create().type(type).build();
    }
}

