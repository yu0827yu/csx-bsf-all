package com.yh.csx.bsf.bigdata.tidb;

import com.yh.csx.bsf.core.util.PropertyUtils;
import com.yh.csx.bsf.core.util.ReflectionUtils;
import lombok.var;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ConditionalOnProperty(name = "bsf.tidb.enabled", havingValue = "true")
public class TidbConfiguration {
    @Bean("tidbDataSource")
    @ConfigurationProperties("spring.datasource.tidb")
    public DataSource tidbDataSource(){
        var type = ReflectionUtils.classForName(PropertyUtils.getPropertyCache("spring.datasource.tidb.type",""));
        return DataSourceBuilder.create().type(type).build();
    }
}

